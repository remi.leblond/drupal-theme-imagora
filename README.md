# Drupal Theme ImAGORA

Thème Drupal utilisé par le site ImAGORA-photo ([imagora-photo.fr](https://imagora-photo.fr)).
Si thème a été initialement mis au point pour la plateforme Drupal 7.